//
//  Network.swift
//  OnlineShop
//
//  Created by Anatolii Kyryliuk on 23/03/2021.
//

import Foundation
import Combine
import UIKit

protocol NetworkProtocol: class {
    typealias Headers = [String: Any]

    func get<T>(type: T.Type, url: URL, headers: Headers) -> AnyPublisher<T, Error> where T: Decodable
    func getImage(from url: URL) -> AnyPublisher<UIImage, Error>
    func post<T, U>(url: URL, headers: Headers, payload: T) -> AnyPublisher<U, Error> where T: Codable, U: Decodable
}

final class Network: NetworkProtocol {
    enum HttpMethods: String {
        case get = "GET"
        case post = "POST"
    }
    func get<T: Decodable>(type: T.Type, url: URL, headers: Headers) -> AnyPublisher<T, Error> {

        var urlRequest = URLRequest(url: url)

        headers.forEach { (key, value) in
            if let value = value as? String {
                urlRequest.setValue(value, forHTTPHeaderField: key)
            }
        }

        return URLSession.shared.dataTaskPublisher(for: urlRequest)
            .map(\.data)
            .decode(type: T.self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }

    func getImage(from url: URL) -> AnyPublisher<UIImage, Error> {
        URLSession.shared.dataTaskPublisher(for: url)
            .map(\.data)
            .tryMap { data in
                guard let image = UIImage(data: data) else {
                    throw URLError(.badServerResponse, userInfo: [
                        NSURLErrorFailingURLErrorKey: url
                    ])
                }
                return image
            }
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }

    func post<T, U>(
        url: URL,
        headers: Headers,
        payload: T) -> AnyPublisher<U, Error> where T: Codable, U: Decodable {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = HttpMethods.post.rawValue

        headers.forEach { (key, value) in
            if let value = value as? String {
                urlRequest.setValue(value, forHTTPHeaderField: key)
            }
        }
        
        let encoder = JSONEncoder()
        do {
            let jsonData = try encoder.encode(payload)
            urlRequest.httpBody = jsonData
        }catch {
            print("\(error.localizedDescription)")
        }

        return URLSession.shared.dataTaskPublisher(for: urlRequest)
            .map(\.data)
            .decode(type: U.self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
}
