//
//  Endpoint.swift
//  OnlineShop
//
//  Created by Anatolii Kyryliuk on 23/03/2021.
//

import Foundation

struct Endpoint {
    var path: String
    var port: Int
    var queryItems: [URLQueryItem] = []
}

extension Endpoint {
    var url: URL {
        var components = URLComponents()
        components.scheme = "http"
        components.host = "localhost"
        components.port = port
        components.path = path
        components.queryItems = queryItems

        guard let url = components.url else {
            preconditionFailure("Invalid URL components: \(components)")
        }

        return url
    }

    var headers: [String: Any] {
        return [
            "Content-Type": "application/json"
        ]
    }
}

extension Endpoint {
    static var products: Self {
        return Endpoint(path: "/product", port: 3001)
    }

    static func product(id: String) -> Self {
        return Endpoint(path: "/product/\(id)", port: 3001)
    }

    static func review(id: String) -> Self {
        return Endpoint(path: "/reviews/\(id)", port: 3002)
    }
}
