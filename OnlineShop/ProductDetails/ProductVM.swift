//
//  ProductVM.swift
//  OnlineShop
//
//  Created by Anatolii Kyryliuk on 23/03/2021.
//

import Foundation
import Combine
import SwiftUI

class ProductVM: ObservableObject {
    private var product: Product {
        didSet {
            updateProperties()
        }
    }

    private let cancelBag = CancelBag()
    private let dataProvider: DataProviderProtocol
    private var imageCache: ImageCacheProtocol?

    var id = UUID()

    @Published var image: Image = Image(systemName: "photo")
    @Published var title: String = ""
    @Published var price: String = ""
    @Published var description: String = ""
    @Published var isShowingNoImplementationAlert: Bool = false

    @Published var dataState: ProductDataState = .ready

    @Published var reviewVMs: [ReviewCellVM] = []
    @Published var isShowingReviewView: Bool = false

    private lazy var isShowingReviewViewBinding = Binding<Bool>(
        get: { self.isShowingReviewView },
        set: { self.isShowingReviewView = $0 }
    )

    lazy var newReviewVM: ReviewViewVM = ReviewViewVM(
        dataProvider: dataProvider,
        productId: product.id,
        isViewPresented: isShowingReviewViewBinding,
        addReview: addReview)

    init(product: Product, dataProvider: DataProviderProtocol) {
        self.dataProvider = dataProvider
        self.product = product

        self.updateProperties()
    }

    func loadImage() {
        dataProvider.getImage(urlString: product.imgUrl)
            .sink(receiveCompletion: { (completion) in
                switch completion {
                case let .failure(error):
                    print("Couldn't get product details: \(error)")
                    self.dataState = .error
                    self.image = Image(systemName: "nosign")
                case .finished: break
                }
            }) { [weak self] image in
                self?.dataState = .ready
                self?.image = Image(uiImage: image)
            }
            .store(in: cancelBag)
    }

    func loadProductDetails() {
        dataState = .loading

        dataProvider.getProduct(id: product.id)
            .sink(receiveCompletion: { (completion) in
                switch completion {
                case let .failure(error):
                    print("Couldn't get product details: \(error)")
                    self.dataState = .error
                case .finished: break
                }
            }) { [weak self] product in
                self?.dataState = .ready
                self?.product = product
            }
            .store(in: cancelBag)
    }

    func addReview() {
        isShowingReviewView = true
    }

    func addToFavourites() {
        isShowingNoImplementationAlert = true
    }

    func buy() {
        isShowingNoImplementationAlert = true
    }

    func addReview(review: Product.Review) {
        product.reviews?.append(review)
        reviewVMs = product.reviews?.map { ReviewCellVM(review: $0) } ?? []
    }

    private func updateProperties() {
        title = product.name
        let currency = !product.currency.isEmpty ? product.currency : "€"
        price = "\(String(format: "%.2f", product.price)) \(currency)"
        description = product.description
        reviewVMs = product.reviews?.map { ReviewCellVM(review: $0) } ?? []
    }
}
