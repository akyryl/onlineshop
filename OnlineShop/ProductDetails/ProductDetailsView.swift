//
//  ProductDetailsView.swift
//  OnlineShop
//
//  Created by Anatolii Kyryliuk on 23/03/2021.
//

import SwiftUI

struct ProductDetailsView: View {
    @ObservedObject var viewModel: ProductVM

    var body: some View {
        ScrollView {
            VStack {
                viewModel.image
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                VStack(alignment: .leading) {
                    HStack {
                    Text(viewModel.price)
                        .font(.title)
                        Spacer()
                        Button(action: {
                            viewModel.addToFavourites()
                        }, label: {
                            Image(systemName: "suit.heart")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 25)
                                .padding(20)
                        })
                        .buttonStyle(PlainButtonStyle())
                        Button(action: {
                            viewModel.buy()
                        }, label: {
                            Text("Buy")
                                .padding(15)
                                .background(Color.black)
                                .foregroundColor(.white)
                        })
                        .buttonStyle(PlainButtonStyle())
                    }
                    Text(viewModel.description)
                        .multilineTextAlignment(.leading)
                }
                .padding(.bottom)
                VStack(alignment: .leading, spacing: 20) {
                    ForEach(viewModel.reviewVMs, id: \.id) { reviewVM in
                        ReviewCell(viewModel: reviewVM)
                        Divider()
                    }
                }
            }
            .alert(isPresented: $viewModel.isShowingNoImplementationAlert) {
                Alert(
                    title: Text("TODO"),
                    message: Text("Not implemented"),
                    dismissButton: .default(Text("OK")))
            }
            .padding()
            .onAppear() {
                viewModel.loadProductDetails()
            }
        }
        .navigationTitle(viewModel.title)
        Button(action: {
            viewModel.addReview()
        }, label: {
            Text("Add review")
                .padding(10)
                .background(Color.white)
                .foregroundColor(.black)
                .border(Color.black)
        }).sheet(isPresented: $viewModel.isShowingReviewView) {
            ReviewView(viewModel: viewModel.newReviewVM)
        }
    }
}

struct ProductDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        ProductDetailsView(viewModel: ProductVM(product: Product.data[0], dataProvider: DataProvider(network: Network(), imageCache: ImageCache())))
    }
}
