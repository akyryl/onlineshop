//
//  ReviewCellVM.swift
//  OnlineShop
//
//  Created by Anatolii Kyryliuk on 24/03/2021.
//

import SwiftUI

struct IdentifiableRateScale: Identifiable {
    var id = UUID()
    var value: Bool
}

class ReviewCellVM: ObservableObject {
    var id = UUID()
    let review: Product.Review

    @Published var text: String
    @Published var starts: [Image] = []
    @Published var sss: [IdentifiableRateScale] = []

    init(review: Product.Review) {
        self.review = review

        self.text = review.text
        for _ in 1...review.rating {
            starts.append(Image(systemName: "star.fill"))
        }
        for _ in review.rating...5 {
            starts.append(Image(systemName: "star.fill"))
        }

        for index in 0..<5 {
            index < review.rating ? sss.append(IdentifiableRateScale(value: true)) : sss.append(IdentifiableRateScale(value: false))
        }
    }
}
