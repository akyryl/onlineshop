//
//  ReviewCell.swift
//  OnlineShop
//
//  Created by Anatolii Kyryliuk on 24/03/2021.
//

import SwiftUI


struct ReviewCell: View {
    @ObservedObject var viewModel: ReviewCellVM

    var body: some View {
        VStack(alignment: .leading) {
            HStack(alignment: .top) {
                ForEach(viewModel.sss) { fillStar in
                    if fillStar.value {
                        Image(systemName: "star.fill")
                            .foregroundColor(.yellow)
                    } else {
                        Image(systemName: "star")
                            .foregroundColor(.yellow)
                    }
                }
            }
            .padding(.bottom)
            Text(viewModel.text)
        }
        .buttonStyle(DefaultButtonStyle())
    }
}
