//
//  ProductCellVM.swift
//  OnlineShop
//
//  Created by Anatolii Kyryliuk on 23/03/2021.
//

import Foundation

class ProductCellVM {
    let product: Product

    init(product: Product) {
        self.product = product
    }
}
