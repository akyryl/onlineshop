//
//  ProductDataState.swift
//  OnlineShop
//
//  Created by Anatolii Kyryliuk on 23/03/2021.
//

import Foundation

enum ProductDataState {
    case loading, ready, error

    var title: String {
        switch self {
        case .loading: return "loading"
        case .ready: return ""
        case .error: return "loading error"
        }
    }
}
