//
//  ProductListView.swift
//  OnlineShop
//
//  Created by Anatolii Kyryliuk on 23/03/2021.
//

import SwiftUI
import Combine

struct ProductListView: View {
    @ObservedObject var viewModel: ProductListVM
    @State private var searchText: String = ""
    @State private var lazyStackId: UUID = UUID()

    var body: some View {
        let searchTextBinding = Binding<String>(get: {
            self.searchText
        }, set: {
            self.searchText = $0
            viewModel.filter(by: self.searchText)
            lazyStackId = UUID()
        })

        NavigationView {
            VStack {
                if viewModel.dataState == .ready {
                    VStack {
                        SearchView(text: searchTextBinding)
                        ScrollView {
                            LazyVStack(alignment: .leading) {
                                ForEach(viewModel.productViewModels, id: \.id) { productVM in
                                    ProductCell(productVM: productVM)
                                }
                            }
                            .id(lazyStackId)
                            .padding()
                        }
                    }
                } else if viewModel.dataState == .error {
                    VStack {
                        Text("Error loading products, please check your connection")
                        Button(action: {
                            viewModel.loadProducts()
                        }, label: {
                            Text("Retry")
                                .padding(15)
                                .background(Color.black)
                                .foregroundColor(.white)
                        })
                    }
                }
                else {
                    if viewModel.dataState == .loading {
                        ProgressView()
                            .progressViewStyle(CircularProgressViewStyle())
                    }
                }
            }
            .navigationTitle("All Products")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ProductListView(viewModel: ProductListVM(dataProvider: DataProvider(network: Network(), imageCache: ImageCache())))
    }
}
