//
//  ProductCell.swift
//  OnlineShop
//
//  Created by Anatolii Kyryliuk on 23/03/2021.
//

import SwiftUI

struct ProductCell: View {
    @ObservedObject var productVM: ProductVM
    
    var body: some View {
        NavigationLink(destination: ProductDetailsView(viewModel: productVM)) {
            ZStack {
                productVM.image
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                VStack(alignment: .trailing) {
                    Spacer()
                    HStack {
                        Text(productVM.price)
                            .padding()
                            .font(.title)
                            .foregroundColor(.black)
                            .background(Color.white.opacity(0.7))
                        Spacer()
                        Button(action: {
                            productVM.addToFavourites()
                        }, label: {
                            Image(systemName: "suit.heart")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 25)
                                .padding(20)
                                .background(Color.white.opacity(0.7))
                                .foregroundColor(.black)
                        })
                        .buttonStyle(PlainButtonStyle())
                    }
                }
                .padding(.bottom)
            }
            .alert(isPresented: $productVM.isShowingNoImplementationAlert) {
                Alert(
                    title: Text("TODO"),
                    message: Text("Not implemented"),
                    dismissButton: .default(Text("OK")))
            }
            .onAppear() {
                productVM.loadImage()
            }
        }
        .buttonStyle(PlainButtonStyle())
    }
}

struct ProductCell_Previews: PreviewProvider {
    static var previews: some View {
        ProductCell(
            productVM: ProductVM(
                product: Product.data[0],
                dataProvider: DataProvider(network: Network(), imageCache: ImageCache())))
    }
}
