//
//  ProductListVM.swift
//  OnlineShop
//
//  Created by Anatolii Kyryliuk on 23/03/2021.
//

import Foundation

class ProductListVM: ObservableObject {
    private let cancelBag = CancelBag()

    private var dataProvider: DataProviderProtocol
    private var products: [Product] = []

    @Published var dataState: ProductDataState = .ready
    @Published var productViewModels: [ProductVM] = []

    init(dataProvider: DataProviderProtocol) {
        self.dataProvider = dataProvider
    }

    func loadProducts() {
        dataState = .loading

        dataProvider.getProducts()
            .sink(receiveCompletion: { (completion) in
                switch completion {
                case let .failure(error):
                    print("Couldn't get products: \(error)")
                    self.productViewModels = []
                    self.dataState = .error
                case .finished: break
                }
            }) { [weak self] products in
                guard let this = self else { return }
                this.dataState = .ready
                this.products = products
                this.productViewModels = products.map { ProductVM(product: $0, dataProvider: this.dataProvider) }
            }
            .store(in: cancelBag)
    }

    func filter(by text: String) {
        guard !text.isEmpty else {
            productViewModels = products.map { ProductVM(product: $0, dataProvider: dataProvider) }
            return
        }

        productViewModels = products
            .filter { $0.name.lowercased().contains(text.lowercased()) }
            .map { ProductVM(product: $0, dataProvider: dataProvider) }
    }
}
