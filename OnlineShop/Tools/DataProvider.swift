//
//  ProductsProvider.swift
//  OnlineShop
//
//  Created by Anatolii Kyryliuk on 23/03/2021.
//

import Foundation
import Combine
import UIKit

protocol DataProviderProtocol: class {
    func getProducts() -> AnyPublisher<[Product], Error>
    func getProduct(id: String) -> AnyPublisher<Product, Error>
    func getImage(urlString: String) -> AnyPublisher<UIImage, Error>
    func addReview(review: Product.Review) -> AnyPublisher<Product.Review, Error>
}

final class DataProvider: DataProviderProtocol {
    enum DataProviderError: Error {
        case invalidUrl
    }

    private let network: NetworkProtocol
    private var imageCache: ImageCacheProtocol

    init(network: NetworkProtocol, imageCache: ImageCacheProtocol) {
        self.network = network
        self.imageCache = imageCache
    }

    func getProducts() -> AnyPublisher<[Product], Error> {
        let endpoint = Endpoint.products

        return network.get(
            type: [Product].self,
            url: endpoint.url,
            headers: endpoint.headers)
    }

    func getProduct(id: String) -> AnyPublisher<Product, Error> {
        let endpoint = Endpoint.product(id: id)

        return network.get(
            type: Product.self,
            url: endpoint.url,
            headers: endpoint.headers)
    }

    func getImage(urlString: String) -> AnyPublisher<UIImage, Error> {
        guard let url = URL(string: urlString) else {
            return Fail<UIImage, Error>(error: DataProviderError.invalidUrl).eraseToAnyPublisher()
        }

        if let image = imageCache[url] {
            return Just(image)
                .setFailureType(to: Error.self)
                .eraseToAnyPublisher()
        }

        return network.getImage(from: url)
            .map { [weak self] image in
                self?.cache(image: image, url: url)
                return image }
            .eraseToAnyPublisher()
    }

    private func cache(image: UIImage?, url: URL) {
        image.map { imageCache[url] = $0 }
    }

    func addReview(review: Product.Review) -> AnyPublisher<Product.Review, Error> {
        let endpoint = Endpoint.review(id: review.productId)

        return network.post(url: endpoint.url, headers: endpoint.headers, payload: review)
    }
}
