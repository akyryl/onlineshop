//
//  ImageLoader.swift
//  OnlineShop
//
//  Created by Anatolii Kyryliuk on 24/03/2021.
//

import Foundation
import UIKit
import Combine

protocol ImageCacheProtocol {
    subscript(_ url: URL) -> UIImage? { get set }
}

struct ImageCache: ImageCacheProtocol {
    private let cache = NSCache<NSURL, UIImage>()

    subscript(_ key: URL) -> UIImage? {
        get { cache.object(forKey: key as NSURL) }
        set { newValue == nil ? cache.removeObject(forKey: key as NSURL) : cache.setObject(newValue!, forKey: key as NSURL) }
    }
}
