//
//  LocalDataProvider.swift
//  OnlineShop
//
//  Created by Anatolii Kyryliuk on 23/04/2021.
//

import Combine
import UIKit

class LocalDataProvider: DataProviderProtocol {
    enum DataProviderError: Error {
        case invalidUrl
        case invalidFileName
        case notImplemented
    }

    func getProducts() -> AnyPublisher<[Product], Error> {
        guard let url = Bundle.main.url(forResource: "products", withExtension: "json"),
              let data = try? Data(contentsOf: url) else {
            return Fail<[Product], Error>(error: DataProviderError.invalidFileName).eraseToAnyPublisher()
        }

        let decoder = JSONDecoder()
        return Just(data)
            .decode(type: [Product].self, decoder: decoder)
            .eraseToAnyPublisher()
    }

    func getProduct(id: String) -> AnyPublisher<Product, Error> {
        return Fail<Product, Error>(error: DataProviderError.notImplemented).eraseToAnyPublisher()
    }

    func addReview(review: Product.Review) -> AnyPublisher<Product.Review, Error> {
        return Fail<Product.Review, Error>(error: DataProviderError.notImplemented).eraseToAnyPublisher()
    }

    func getImage(urlString: String) -> AnyPublisher<UIImage, Error> {
        guard let image = UIImage(named: "image.jpg") else {
            return Fail<UIImage, Error>(error: DataProviderError.invalidFileName).eraseToAnyPublisher()
        }
        return Just(image)
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
    }
}
