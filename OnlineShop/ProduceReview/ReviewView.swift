//
//  ReviewView.swift
//  OnlineShop
//
//  Created by Anatolii Kyryliuk on 25/03/2021.
//

import SwiftUI

struct ReviewView: View {
    @ObservedObject var viewModel: ReviewViewVM

    var body: some View {
        VStack {
            ZStack {
                Text("Your review")
                    .font(.title)
                HStack {
                    Spacer()
                    Button(action: {
                        viewModel.dismiss()
                    }, label: {
                        Image(systemName: "xmark")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 25)
                            .padding(20)
                    })
                    .buttonStyle(PlainButtonStyle())
                }
            }
            HStack {
                ForEach(0 ..< viewModel.maxRating) { i in
                    Button(action: {
                        viewModel.reviewRating = i + 1
                    }, label: {
                        if viewModel.reviewRating - 1 >= i {
                            Image(systemName: "star.fill")
                                .foregroundColor(.yellow)
                        } else {
                            Image(systemName: "star")
                                .foregroundColor(.yellow)
                        }
                    })
                    .buttonStyle(PlainButtonStyle())
                }
            }
            TextEditor(text: $viewModel.reviewText)
                .border(Color.black)
                .padding()
            Spacer()
            if viewModel.isSubmitting {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle())
            } else if viewModel.isShowingDoneAlert {
                if viewModel.isShowingDoneAlert {
                    Text("Thanks for your feedback!")
                        .frame(maxWidth: .infinity)
                        .padding()
                        .background(Color.green)
                        .foregroundColor(.white)
                }
            } else {
                Button(action: {
                    viewModel.submitReview()
                }, label: {
                    Text("Submit")
                        .padding(15)
                        .background(viewModel.reviewText.isEmpty ? Color.gray : Color.black)
                        .foregroundColor(.white)
                })
                .disabled(viewModel.reviewText.isEmpty)
                .padding()
            }
        }
        .alert(isPresented: $viewModel.isShowingErrorAlert) {
            Alert(
                title: Text("Unable to send your feedback"),
                message: Text("Please check your internet connection and try again"),
                dismissButton: .default(Text("OK")))
        }
    }
}

struct ReviewView_Previews: PreviewProvider {
    static var previews: some View {
        ReviewView(
            viewModel: ReviewViewVM(
                dataProvider: DataProvider(network: Network(), imageCache: ImageCache()),
                productId: "0", isViewPresented: .constant(false),
                addReview: nil))
    }
}
