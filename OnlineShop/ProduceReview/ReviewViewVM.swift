//
//  ReviewViewVM.swift
//  OnlineShop
//
//  Created by Anatolii Kyryliuk on 25/03/2021.
//

import SwiftUI
import Combine

class ReviewViewVM: ObservableObject {
    @Published var reviewText: String = ""
    @Published var reviewRating: Int = 5
    @Published var isShowingDoneAlert: Bool = false
    @Published var isShowingErrorAlert: Bool = false
    @Published var isSubmitting: Bool = false

    @Binding var isViewPresented: Bool

    var maxRating: Int = Product.Review.maxRating
    var submittedReview: Product.Review?

    private let dataProvider: DataProviderProtocol
    private let cancelBag = CancelBag()
    private let productId: String
    private var timerCancellable: AnyCancellable?

    typealias AddReviewHandler = (Product.Review) -> Void

    var addReview: AddReviewHandler?

    init(dataProvider: DataProviderProtocol,
         productId: String,
         isViewPresented: Binding<Bool>,
         addReview: AddReviewHandler?) {
        self.dataProvider = dataProvider
        self.productId = productId
        self._isViewPresented = isViewPresented
        self.addReview = addReview
    }

    func submitReview() {
        let newReview = Product.Review(
            productId: productId,
            locale: Locale.current.languageCode ?? "",
            rating: reviewRating,
            text: reviewText)

        isSubmitting = true
        dataProvider.addReview(review: newReview)
            .sink(receiveCompletion: { [weak self] (completion) in
                self?.isSubmitting = false
                switch completion {
                case let .failure(error):
                    print("Error submitting a review: \(error)")
                    self?.isShowingErrorAlert = true
                case .finished: break
                }
            }) { [weak self] submittedReview in
                print("Review submitted: \(submittedReview)")

                self?.submittedReview = submittedReview
                self?.addReview?(submittedReview)
                self?.showAutohideToast()
            }
            .store(in: cancelBag)
    }

    func dismiss() {
        isViewPresented = false
        resetViewData()
        timerCancellable?.cancel()
    }

    private func showAutohideToast() {
        isShowingDoneAlert = true
        timerCancellable = Timer.publish(every: 1, on: .main, in: .default)
            .autoconnect()
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { _ in
                self.dismiss()
            })
    }

    private func resetViewData() {
        reviewText = ""
        isShowingDoneAlert = false
        isShowingErrorAlert = false
        isSubmitting = false
    }
}
