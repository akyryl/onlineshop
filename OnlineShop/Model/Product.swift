//
//  Product.swift
//  OnlineShop
//
//  Created by Anatolii Kyryliuk on 23/03/2021.
//

import Foundation

struct Product: Codable {
    struct Review: Codable {
        static let maxRating = 5

        let productId: String
        let locale: String
        let rating: Int
        let text: String
    }

    let id: String
    let name: String
    let description: String
    let currency: String
    let price: Float
    let imgUrl: String
    var reviews: [Review]?
}

extension Product {
    static var data: [Product] {
        [
            Product(id: "0", name: "Product 1", description: "Descrition 1", currency: "", price: 200.0, imgUrl: "no inage", reviews: nil),
            Product(id: "1", name: "Product 2", description: "Descrition 2", currency: "", price: 200.0, imgUrl: "no inage", reviews: nil),
            Product(id: "2", name: "Product 3", description: "Descrition 3", currency: "", price: 200.0, imgUrl: "no inage", reviews: nil)
        ]
    }
}
