//
//  OnlineShopApp.swift
//  OnlineShop
//
//  Created by Anatolii Kyryliuk on 23/03/2021.
//

import SwiftUI

@main
struct OnlineShopApp: App {
    @State private var viewModel = ProductListVM(
        // dataProvider: DataProvider(network: Network(), imageCache: ImageCache()))
        dataProvider: LocalDataProvider())

    var body: some Scene {
        WindowGroup {
            ProductListView(viewModel: viewModel)
                .onAppear {
                    viewModel.loadProducts()
                }
        }
    }
}
