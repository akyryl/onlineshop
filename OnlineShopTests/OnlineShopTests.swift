//
//  OnlineShopTests.swift
//  OnlineShopTests
//
//  Created by Anatolii Kyryliuk on 23/03/2021.
//

import XCTest
import Combine

@testable import OnlineShop

class OnlineShopTests: XCTestCase {
    func testDataProvider() throws {
        let network = TestNetwork()
        let noImageCache = TestNoImageCache()
        let cancelBag = CancelBag()

        let dataProvider = DataProvider(network: network, imageCache: noImageCache)
        dataProvider.getProducts()
            .sink(receiveCompletion: { (completion) in
                switch completion {
                case let .failure(error):
                    XCTFail("Error loading data from provider: \(error)")
                case .finished: break
                }
            }) { products in
                XCTAssert(network.getCallsCounter == 1, "Amount of get network call is invalid: \(network.getCallsCounter)")
            }
            .store(in: cancelBag)

        let urlString = "https://assets.test.com"
        dataProvider.getImage(urlString: urlString)
            .sink(receiveCompletion: { (completion) in
                switch completion {
                case let .failure(error):
                    XCTFail("Error loading image from provider: \(error)")
                case .finished: break
                }
            }) { image in
                XCTAssert(network.getImageCallsCounter == 1, "Amount of get network call is invalid: \(network.getCallsCounter)")
                XCTAssert(noImageCache.getCallsCounter == 1, "Amount of get cache call is invalid: \(noImageCache.getCallsCounter)")
                XCTAssert(noImageCache.setCallsCounter == 1, "Amount of get cache call is invalid: \(noImageCache.setCallsCounter)")
            }
            .store(in: cancelBag)

        let oneImageCache = TestOneImageCache()

        let dataProvider2 = DataProvider(network: network, imageCache: oneImageCache)
        dataProvider2.getImage(urlString: urlString)
            .sink(receiveCompletion: { (completion) in
                switch completion {
                case let .failure(error):
                    XCTFail("Error loading image from provider: \(error)")
                case .finished: break
                }
            }) { image in
                XCTAssert(network.getImageCallsCounter == 1, "Amount of get network call is invalid: \(network.getCallsCounter)")
                XCTAssert(oneImageCache.getCallsCounter == 1, "Amount of get cache call is invalid: \(oneImageCache.getCallsCounter)")
                XCTAssert(oneImageCache.setCallsCounter == 0, "Amount of get cache call is invalid: \(oneImageCache.setCallsCounter)")
            }
            .store(in: cancelBag)
    }

    func testProductListVM() {
        let viewModel = ProductListVM(dataProvider: MockDataProvider())
        viewModel.loadProducts()
        viewModel.filter(by: "P")
        let _ = viewModel.$productViewModels.sink(receiveValue: { items in
            XCTAssert(items.count == 3, "Products filtering returns wrong amoung of items: \(items.count)")
        })

        viewModel.filter(by: "product")
        let _ = viewModel.$productViewModels.sink(receiveValue: { items in
            XCTAssert(items.count == 2, "Products filtering returns wrong amoung of items: \(items.count)")
        })

        viewModel.filter(by: "product ONE")
        let _ = viewModel.$productViewModels.sink(receiveValue: { items in
            XCTAssert(items.count == 1, "Products filtering returns wrong amoung of items: \(items.count)")
        })

        viewModel.filter(by: "NO SUHCH PRODUCT")
        let _ = viewModel.$productViewModels.sink(receiveValue: { items in
            XCTAssert(items.count == 0, "Products filtering returns wrong amoung of items: \(items.count)")
        })
    }
}
