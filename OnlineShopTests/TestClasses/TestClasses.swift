//
//  TestClasses.swift
//  OnlineShopTests
//
//  Created by Anatolii Kyryliuk on 26/03/2021.
//

import Foundation
import Combine
import UIKit

@testable import OnlineShop

class TestNetwork: NetworkProtocol {
    static let productsJsonStr =
        """
        [
            {
                "currency": "",
                "price": 0,
                "id": "FI444",
                "name": "product",
                "description": "description",
                "imgUrl": "https://assets.adidas.com/images/w_320,h_320,f_auto,q_auto:sensitive,fl_lossy/6634cf36274b4ea5ac46ac4e00b2021e_9366/Superstar_Shoes_Black_FY0071_01_standard.jpg",
                "reviews": [
                  {
                    "productId": "FI444",
                    "locale": "en-US",
                    "rating": 2,
                    "text": "this product is the bestaaaa"
                  }
                ]
              }
        ]
        """

    var getCallsCounter: Int = 0
    var postCallsCounter: Int = 0
    var getImageCallsCounter: Int = 0

    func get<T>(type: T.Type, url: URL, headers: Headers) -> AnyPublisher<T, Error> where T : Decodable {
        getCallsCounter += 1
        let result = try! JSONDecoder().decode(T.self, from: TestNetwork.productsJsonStr.data(using: .utf8)!)
        return Just(result)
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
    }

    func getImage(from url: URL) -> AnyPublisher<UIImage, Error> {
        getImageCallsCounter += 1
        return Just(UIImage())
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
    }

    func post<T, U>(url: URL, headers: Headers, payload: T) -> AnyPublisher<U, Error> where T : Decodable, T : Encodable, U : Decodable {
        postCallsCounter += 1
        let result = try! JSONDecoder().decode(U.self, from: TestNetwork.productsJsonStr.data(using: .utf8)!)
        return Just(result)
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
    }
}

class TestNoImageCache: ImageCacheProtocol {
    var getCallsCounter: Int = 0
    var setCallsCounter: Int = 0

    subscript(url: URL) -> UIImage? {
        get {
            getCallsCounter += 1
            return nil
        }
        set(newValue) {
            setCallsCounter += 1
        }
    }
}

class TestOneImageCache: ImageCacheProtocol {
    var getCallsCounter: Int = 0
    var setCallsCounter: Int = 0

    subscript(url: URL) -> UIImage? {
        get {
            getCallsCounter += 1
            return UIImage()
        }
        set(newValue) {
            setCallsCounter += 1
        }
    }
}
