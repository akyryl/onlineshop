//
//  MockDataProvider.swift
//  OnlineShopTests
//
//  Created by Anatolii Kyryliuk on 26/03/2021.
//

import Foundation
import Combine
import UIKit

@testable import OnlineShop

class MockDataProvider: DataProviderProtocol {
    // TODO: Better to have mock json file
    static let products = [
        Product(id: "0", name: "Product one", description: "Descrition 1", currency: "", price: 200.0, imgUrl: "no inage", reviews: nil),
        Product(id: "1", name: "producT two", description: "Descrition 2", currency: "", price: 200.0, imgUrl: "no inage", reviews: nil),
        Product(id: "2", name: "prior", description: "Descrition 3", currency: "", price: 200.0, imgUrl: "no inage", reviews: nil)]

    func getProducts() -> AnyPublisher<[Product], Error> {
        return Just(MockDataProvider.products)
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
    }

    func getProduct(id: String) -> AnyPublisher<Product, Error> {
        return Just(MockDataProvider.products[0])
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
    }

    func getImage(urlString: String) -> AnyPublisher<UIImage, Error> {
        return Just(UIImage())
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
    }

    func addReview(review: Product.Review) -> AnyPublisher<Product.Review, Error> {
        return Just(Product.Review(productId: "1", locale: "en", rating: 4, text: "Great!"))
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
    }
}
